﻿using Dummiesman;
using System.IO;
using System.Text;
using UnityEngine;

public class ObjFromStream : MonoBehaviour {
    // Lista de objetos de prueba
    // https://people.sc.fsu.edu/~jburkardt/data/obj/
    // NOTA: Se debe ingresar SÓLO EL NOMBRE del objeto dentro del programa

	public static void Start () {

        //make www
        var www = new WWW("https://people.sc.fsu.edu/~jburkardt/data/obj/" + CharacterCreator.charName + ".obj");

        while (!www.isDone)
            System.Threading.Thread.Sleep(1);
        
        //crear un stream y cargar
        var textStream = new MemoryStream(Encoding.UTF8.GetBytes(www.text));
        var loadedObj = new OBJLoader().Load(textStream);
	}
}

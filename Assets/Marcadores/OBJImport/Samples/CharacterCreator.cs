﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharacterCreator : MonoBehaviour {

    // Name Input Field
    public InputField nameField;

    //Character name string
    public static string charName;

    public void OnSubmit() {
        //Set charName string to text in namefield.
        charName = nameField.text;

        // Display character name in console
        Debug.Log("Seleccionaste: " + charName);
        if (GameObject.Find("WavefrontObject") != null) {
            Destroy(GameObject.Find("WavefrontObject"));
            ObjFromStream.Start();
        } else {
            ObjFromStream.Start();
        }
    }
}

# Visor de resultados de impresión 3D con Realidad Aumentada

![](https://lh3.googleusercontent.com/7FxN8SNOfZXXZcr76rxEpZygvhfpDn1MJyCJPjiwB1_5Lb-62RYFnf6JMlKdthv-p23jIXWL0g4L0syiKyW7ZlosjeDQC0x-GTZtYkkvAohjnklZTWNAl3amgTT7e6vCMzz6DVdI)

## Introducción

La realidad aumentada y la impresión 3D, aunque son tecnologías muy diferentes, giran alrededor del mismo concepto general: cerrar la brecha entre el mundo físico y el mundo digital. La impresión 3D hace que lo digital sea físico y la realidad aumentada mejora el mundo físico con superposiciones digitales.

La combinación de estas dos tecnologías puede ayudar a que los usuarios se hagan una idea de lo que van a obtener una vez que ponga en marcha su impresora 3D y quizá aplicar modificaciones al diseño en pleno proceso de desarrollo del mismo.

El concepto VisoRA, permite a los propietarios de impresoras 3D obtener una vista previa en tiempo real de sus diseños 3D a medida que los crean sus dispositivos. Junto con la superposición de diseño, también muestra información como cuánto tiempo ha estado funcionando la impresora y, en teoría, cuánto tiempo se necesitará un trabajo para completar. Puedes ver un ejemplo de cómo funciona esto en el video a continuación. [Ultimaker 3D printing & augmented reality](http://www.youtube.com/watch?v=8lk9GhU36b4)

## Instrucciones

Para poder lograr esto el usuario deberá subir su modelo 3D a nuestra base de datos de C&S, el cual se encargará de comunicarse con el dispositivo móvil para proporcionarle dicho modelo y finalmente mostrar el resultado final en la plataforma de impresión 3D a través de un escaneo de nuestra plantilla personalizada.

# Creando un proyecto de Realidad Aumentada en Unity

Para empezar a crear un proyecto con Unity y Vuforia es necesario que repasemos las instrucciones detalladas en esta página:

## Creando los ImageTargets

Para poder continuar con el proyecto necesitaremos un par de cosas que las vamos a conseguir de manera muy rápida y sencilla:

La primera cosa es una imagen (TARGET IMAGE) que nos va a ser para posicionar nuestros objetos en nuestro entorno a través de la cámara.
Le indircaremos a Vuforia y Unity donde queremos desplegar nuestros objetos en nuestro espacio en la vida real.

Si yo pongo esta imagen en el escritorio, al momento de grabar con la cámara los objetos saldrán de dicha imagen. De otra forma el sistema no sabría donde posicionar los objetos.

Esta imagen puede ser cualquier imagen personalizada, siempre y cuando sea bien contrastada con el entorno donde se ubique.

![VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/LogoHSmallV3_b.png](VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/LogoHSmallV3_b.png)

La segunda será nuestra database, una base de datos que será utilizada para todos los Targets que estaremos usando para realidad aumentada. Una vez que obtuvimos la imagen que se va a utilizar debemos ingresar al portal de Vuforia para poder subirlo a su database:
[https://developer.vuforia.com/vui/develop/databases](https://developer.vuforia.com/vui/develop/databases)

![VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled.png](VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled.png)

Le daremos un nombre a nuestra Database y luego **CREAR.**

![VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%201.png](VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%201.png)

Como veremos nuestra database está vacía, así que debemos agregar nuestro target dentro.

![VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%202.png](VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%202.png)

Nos abrirá un menú en el cual debemos indicar:
1. Que tipo de objeto vamos a subir. En este caso será una **Single Image** donde estará nuestra imagen.
2. La localización de nuestra imagen.
3. El ancho en proporción de la imagen y luego **ADD.**

![VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%203.png](VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%203.png)

Una vez que agregamos nuestro target image, debemos descargarla desde el botón **DOWNLOAD DATABASE (ALL).** El cual nos abrirá esta ventana de exportación:

![VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%204.png](VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%204.png)

Dentro de nuestro proyecto en Unity importaremos el paquete que descargamos, desde el menú `ASSETS > IMPORT PACKAGE > CUSTOM PACKAGE...`. Localizamos nuestro archivo y le damos **ABRIR** y luego **IMPORTAR.**

Ahora podemos ver que en nuestro panel **Projects** se creó una carpeta llamada **StreamingAssets** el cual contiene una carpeta llamada **Vuforia** el cual tendrá todos los targets que usaremos en nuestro proyecto de realidad aumentada.

![VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%205.png](VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%205.png)

![VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%206.png](VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%206.png)

Procedemos a incluir estos targets haciendo `click secundario en el panel Hierarchy > Vuforia Engine > Image Target`.

El Image Target es la imagen que acabamos de generar como paquete para importarlo desde Unity.
A continuación debemos seleccionar nuestro objeto **Image Target** y cambiar sus propiedades en el panel **Inspector.**

Seleccionamos la opción **Type** y cambiaremos su valor por **From Database** y la opción **Database** por el nombre de nuestro Database.

![VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%207.png](VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%207.png)

Veremos que se muestra correctamente nuestro Image Target seleccionado en el visor.

![VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%208.png](VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%208.png)

## Incorporando el modelo

Una vez que tengamos esto es momento de añadirle el tipo de objeto que queremos que se muestre por encima de nuestro Image Target. Se puede variar entre modelos 2D, 3D, imagenes, efectos, luces, audio, video, texto y botones.
Para nuestro proyecto incorporaremos un modelo en 3D almacenado en la nube para luego interactuar desde la interfaz, por lo que necesitaremos importar la extensión OBJImporter desde el Asset Store de Unity:

NOTA: En el caso de que no se visualice ese panel debemos presionar CTRL+9 o ir a Window > Asset Store

![VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%209.png](VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%209.png)

Una vez importada la extensión debemos crear un GameObject llamado **CharacterCreator** dentro del panel **Hierarchy** y luego en el botón **Add Component** agregaremos nuestro script con su mismo nombre.

Como podemos ver en los archivos de nuestro proyecto, se creó un script llamado **CharacterCreator**. Debemos mover este script a la carpeta `OBJImport > Samples` y abrir el archivo.

![VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%2010.png](VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%2010.png)

![VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%2011.png](VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%2011.png)

Dentro de nuestro archivo **CharacterCreator.cs** incorporaremos nuestro código:

```csharp
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharacterCreator : MonoBehaviour {

    // Name Input Field
    public InputField nameField;

    //Character name string
    public static string charName;

    public void OnSubmit() {
        //Set charName string to text in namefield.
        charName = nameField.text;

        // Display character name in console
        Debug.Log("Seleccionaste: " + charName);
        if (GameObject.Find("WavefrontObject") != null) {
            Destroy(GameObject.Find("WavefrontObject"));
            ObjFromStream.Start();
        } else {
            ObjFromStream.Start();
        }
    }
}
```

Además, también deberíamos cambiar algunas propiedades del script **OBJFromStream.cs**

```csharp
using Dummiesman;
using System.IO;
using System.Text;
using UnityEngine;

public class ObjFromStream : MonoBehaviour {
    // Lista de objetos de prueba
    // https://people.sc.fsu.edu/~jburkardt/data/obj/
    // NOTA: Se debe ingresar SÓLO EL NOMBRE del objeto dentro del programa

	public static void Start () {

        //make www
        var www = new WWW("https://people.sc.fsu.edu/~jburkardt/data/obj/" + CharacterCreator.charName + ".obj");

        while (!www.isDone)
            System.Threading.Thread.Sleep(1);
        
        //crear un stream y cargar
        var textStream = new MemoryStream(Encoding.UTF8.GetBytes(www.text));
        var loadedObj = new OBJLoader().Load(textStream);
	}
}
```

Y por último, debemos cambiar unos parámetros de configuración del modelo, tales como el nombre, tamaño y posición. (Dejaré el código completo para hacerlo más rápido)

```csharp
/*
 * Copyright (c) 2019 Dummiesman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
*/

using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System;
using Dummiesman;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Dummiesman
{
    public enum SplitMode {
        None,
        Object,
        Material
    }
    
    public class OBJLoader
    {
        //options
        /// <summary>
        /// Determines how objects will be created
        /// </summary>
        public SplitMode SplitMode = SplitMode.Object;

        //global lists, accessed by objobjectbuilder
        internal List<Vector3> Vertices = new List<Vector3>();
        internal List<Vector3> Normals = new List<Vector3>();
        internal List<Vector2> UVs = new List<Vector2>();

        //materials, accessed by objobjectbuilder
        internal Dictionary<string, Material> Materials;

        //file info for files loaded from file path, used for GameObject naming and MTL finding
        private FileInfo _objInfo;

#if UNITY_EDITOR
        [MenuItem("GameObject/Import From OBJ")]
        static void ObjLoadMenu()
        {
            string pth =  EditorUtility.OpenFilePanel("Import OBJ", "", "obj");
            if (!string.IsNullOrEmpty(pth))
            {
                System.Diagnostics.Stopwatch s = new System.Diagnostics.Stopwatch();
                s.Start();

                var loader = new OBJLoader
                {
                    SplitMode = SplitMode.Object,
                };
                loader.Load(pth);

                Debug.Log($"OBJ import time: {s.ElapsedMilliseconds}ms");
                s.Stop();
            }
        }
#endif

        /// <summary>
        /// Helper function to load mtllib statements
        /// </summary>
        /// <param name="mtlLibPath"></param>
        private void LoadMaterialLibrary(string mtlLibPath)
        {
            if (_objInfo != null)
            {
                if (File.Exists(Path.Combine(_objInfo.Directory.FullName, mtlLibPath)))
                {
                    Materials = new MTLLoader().Load(Path.Combine(_objInfo.Directory.FullName, mtlLibPath));
                    return;
                }
            }

            if (File.Exists(mtlLibPath))
            {
                Materials = new MTLLoader().Load(mtlLibPath);
                return;
            }
        }

        /// <summary>
        /// Load an OBJ file from a stream. No materials will be loaded, and will instead be supplemented by a blank white material.
        /// </summary>
        /// <param name="input">Input OBJ stream</param>
        /// <returns>Returns a GameObject represeting the OBJ file, with each imported object as a child.</returns>
        public GameObject Load(Stream input)
        {
            var reader = new StreamReader(input);
            //var reader = new StringReader(inputReader.ReadToEnd());

            Dictionary<string, OBJObjectBuilder> builderDict = new Dictionary<string, OBJObjectBuilder>();
            OBJObjectBuilder currentBuilder = null;
            string currentMaterial = "default";

            //lists for face data
            //prevents excess GC
            List<int> vertexIndices = new List<int>();
            List<int> normalIndices = new List<int>();
            List<int> uvIndices = new List<int>();

            //helper func
            Action<string> setCurrentObjectFunc = (string objectName) =>
            {
                if (!builderDict.TryGetValue(objectName, out currentBuilder))
                {
                    currentBuilder = new OBJObjectBuilder(objectName, this);
                    builderDict[objectName] = currentBuilder;
                }
            };

            //create default object
            setCurrentObjectFunc.Invoke("default");

			//var buffer = new DoubleBuffer(reader, 256 * 1024);
			var buffer = new CharWordReader(reader, 4 * 1024);

			//do the reading
			while (true)
            {
				buffer.SkipWhitespaces();

				if (buffer.endReached == true) {
					break;
				}

				buffer.ReadUntilWhiteSpace();
				
                //comment or blank
                if (buffer.Is("#"))
                {
					buffer.SkipUntilNewLine();
                    continue;
                }
				
				if (Materials == null && buffer.Is("mtllib")) {
					buffer.SkipWhitespaces();
					buffer.ReadUntilNewLine();
					string mtlLibPath = buffer.GetString();
					LoadMaterialLibrary(mtlLibPath);
					continue;
				}
				
				if (buffer.Is("v")) {
					Vertices.Add(buffer.ReadVector());
					continue;
				}

				//normal
				if (buffer.Is("vn")) {
                    Normals.Add(buffer.ReadVector());
                    continue;
                }

                //uv
				if (buffer.Is("vt")) {
                    UVs.Add(buffer.ReadVector());
                    continue;
                }

                //new material
				if (buffer.Is("usemtl")) {
					buffer.SkipWhitespaces();
					buffer.ReadUntilNewLine();
					string materialName = buffer.GetString();
                    currentMaterial = materialName;

                    if(SplitMode == SplitMode.Material)
                    {
                        setCurrentObjectFunc.Invoke(materialName);
                    }
                    continue;
                }

                //new object
                if ((buffer.Is("o") || buffer.Is("g")) && SplitMode == SplitMode.Object) {
                    buffer.ReadUntilNewLine();
                    string objectName = buffer.GetString(1);
                    setCurrentObjectFunc.Invoke(objectName);
                    continue;
                }

                //face data (the fun part)
                if (buffer.Is("f"))
                {
                    //loop through indices
                    while (true)
                    {
						bool newLinePassed;
						buffer.SkipWhitespaces(out newLinePassed);
						if (newLinePassed == true) {
							break;
						}

                        int vertexIndex = int.MinValue;
                        int normalIndex = int.MinValue;
                        int uvIndex = int.MinValue;

						vertexIndex = buffer.ReadInt();
						if (buffer.currentChar == '/') {
							buffer.MoveNext();
							if (buffer.currentChar != '/') {
								uvIndex = buffer.ReadInt();
							}
							if (buffer.currentChar == '/') {
								buffer.MoveNext();
								normalIndex = buffer.ReadInt();
							}
						}

                        //"postprocess" indices
                        if (vertexIndex > int.MinValue)
                        {
                            if (vertexIndex < 0)
                                vertexIndex = Vertices.Count - vertexIndex;
                            vertexIndex--;
                        }
                        if (normalIndex > int.MinValue)
                        {
                            if (normalIndex < 0)
                                normalIndex = Normals.Count - normalIndex;
                            normalIndex--;
                        }
                        if (uvIndex > int.MinValue)
                        {
                            if (uvIndex < 0)
                                uvIndex = UVs.Count - uvIndex;
                            uvIndex--;
                        }

                        //set array values
                        vertexIndices.Add(vertexIndex);
                        normalIndices.Add(normalIndex);
                        uvIndices.Add(uvIndex);
                    }

                    //push to builder
                    currentBuilder.PushFace(currentMaterial, vertexIndices, normalIndices, uvIndices);

                    //clear lists
                    vertexIndices.Clear();
                    normalIndices.Clear();
                    uvIndices.Clear();

					continue;
                }

				buffer.SkipUntilNewLine();
            }

            GameObject obj = new GameObject(_objInfo != null ? Path.GetFileNameWithoutExtension(_objInfo.Name) : "WavefrontObject");
            obj.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);

            foreach (var builder in builderDict)
            {
                if (builder.Value.PushedFaceCount == 0)
                    continue;

                var builtObj = builder.Value.Build();
                builtObj.transform.SetParent(obj.transform, true);
            }
            // Convierte al objeto en un hijo del padre ImageTarget2
            obj.transform.parent = GameObject.Find("ImageTarget").transform;
            return obj;
        }

        /// <summary>
        /// Load an OBJ and MTL file from a stream.
        /// </summary>
        /// <param name="input">Input OBJ stream</param>
        /// /// <param name="mtlInput">Input MTL stream</param>
        /// <returns>Returns a GameObject represeting the OBJ file, with each imported object as a child.</returns>
        public GameObject Load(Stream input, Stream mtlInput)
        {
            var mtlLoader = new MTLLoader();
            Materials = mtlLoader.Load(mtlInput);

            return Load(input);
        }

        /// <summary>
        /// Load an OBJ and MTL file from a file path.
        /// </summary>
        /// <param name="path">Input OBJ path</param>
        /// /// <param name="mtlPath">Input MTL path</param>
        /// <returns>Returns a GameObject represeting the OBJ file, with each imported object as a child.</returns>
        public GameObject Load(string path, string mtlPath)
        {
            _objInfo = new FileInfo(path);
            if (!string.IsNullOrEmpty(mtlPath) && File.Exists(mtlPath))
            {
                var mtlLoader = new MTLLoader();
                Materials = mtlLoader.Load(mtlPath);

                using (var fs = new FileStream(path, FileMode.Open))
                {
                    return Load(fs);
                }
            }
            else
            {
                using (var fs = new FileStream(path, FileMode.Open))
                {
                    return Load(fs);
                }
            }
        }

        /// <summary>
        /// Load an OBJ file from a file path. This function will also attempt to load the MTL defined in the OBJ file.
        /// </summary>
        /// <param name="path">Input OBJ path</param>
        /// <returns>Returns a GameObject represeting the OBJ file, with each imported object as a child.</returns>
        public GameObject Load(string path)
        {
            return Load(path, null);
        }
    }
}
```

Una vez que hayamos creado nuestros scripts, seleccionaremos nuestro objeto **CharacterCreator** y buscaremos el script con su mismo nombre.
Dentro de su campo **Name Field** seleccionaremos **InputField** el cual será nuestra entrada.

![VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%2012.png](VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%2012.png)

![VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%2013.png](VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%2013.png)

Solo nos falta crear la UI para poder enviar el nombre de nuestro objeto 3D y mostrarlo por pantalla. Por lo que tendremos que ir a nuestro panel **Hierarchy** y crear estos objetos haciendo `Click secundario > UI > Canvas`.
**Dentro de nuestro objeto canvas** debemos crear un `InputField` y un `Button` siguiendo el mismo procedimiento.

Ahora tendremos que arrastrar nuestro objeto **CharacterCreator** desde nuestro objeto Button. Es decir, debemos pararnos en nuestro objeto Button y arrastrar nuestro objeto **CharacterCreator** hasta el método On Click(). En la lista desplegable debemos seleccionar `CharacterCreator > OnSubmit()`.

![VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%2014.png](VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%2014.png)

# Ejecutar el proyecto

Ahora estamos listos para hacer las pruebas. Para poder ejecutar el proyecto en Unity y tener una vista previa mientras se trabaja, solo basta con hacer click en el reproductor de la barra de herramientas.

Para facilitar el acceso a la lista de modelos disponibles les dejo una que conseguí en internet de libre uso.
[https://people.sc.fsu.edu/~jburkardt/data/obj/](https://people.sc.fsu.edu/~jburkardt/data/obj/)

En la siguiente captura podemos ver un ejemplo con el modelo shuttle de esta misma lista.

![VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%2015.png](VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%2015.png)

NOTA: Cuando aceptemos el nombre de nuestro modelo, este se posicionará en el medio de la pantalla, por lo que habrá que posicionar bien nuestra imagen con la cámara.

# Exportando ejecutable para Android

Requisitos: Tener instalado el JDK para Android.
[https://docs.unity3d.com/es/530/Manual/android-sdksetup.html](https://docs.unity3d.com/es/530/Manual/android-sdksetup.html)

Para poder exportar nuestro proyecto a Android debemos presionar las teclas `CTRL + SHIFT + B` o bien ir a `FILE > BUILD SETTINGS...`.

Esto nos abrirá una ventana con las opciones de exportación para los diferentes tipos de sistemas soportados.

Por defecto vendrá seleccionada el soporte para **PC, Mac & Linux Standalone** así que seleccionaremos el sistema operativo Android y le daremos click en **Switch Plataform,** luego click en **Build** y empezará a compilarse nuestro archivo dicho sistema.

![VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%2016.png](VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%2016.png)

## Problemas al compilar

En caso de que Unity muestre un aviso por un problema al compilar probablemente será porque no está instalada (o no encuentra) el JDK requerido para esta acción.
Para poder acceder al menú de configuración nos dirijiremos a `Edit > Preferences...` y colocaremos la ruta completa de nuestro JDK.

![VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%2017.png](VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%2017.png)

## Ejecutando en Android

Para poder instalar nuestro archivo APK en nuestro dispositivo Android es neceseario que se habiliten las opciones de instalación de archivos de **Orígenes desconocidos** dentro del apartado de configuración **BLOQUEO Y SEGURIDAD.**

![VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%2018.png](VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%2018.png)

Una vez que tengamos habilitada esta opción buscamos nuestro archivo en la carpeta correspondiente, lo instalamos y lo ejecutamos.

![VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%2019.png](VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%2019.png)

![VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%2020.png](VisoRA%20a521ed59bc57450c8ab37f08e0c9acad/Untitled%2020.png)

# Pruebelo usted mismo

Descarge el archivo Prueba.apk e instalelo en su dispositivo Android. ¡Escanée las imagenes y explore!.
